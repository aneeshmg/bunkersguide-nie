package com.bunkersguide.nie;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase.CursorFactory;

public class dbStuff {

   public static final String MYDATABASE_NAME = "bunkersnie";
   public static final String MYDATABASE_TABLE = "guide";
   public static final int MYDATABASE_VERSION = 1;
   public static final String KEY_ID = "id";
   public static final String KEY_SUBJECT = "subject";
   public static final String KEY_BUNKED = "bunked";
   public static final String KEY_CPS = "cps";
   public static final String KEY_M1 = "m1";
   public static final String KEY_M2 = "m2";
   public static final String KEY_M3 = "m3";

   private static final String SCRIPT_CREATE_DATABASE = "create table " + MYDATABASE_TABLE + " (" + KEY_ID + " integer primary key autoincrement, "
                                                         + KEY_SUBJECT + " text not null, " + KEY_BUNKED + " integer, " + KEY_CPS + 
                                                         " integer, " + KEY_M1 + " integer, " + KEY_M2 + " integer, " + KEY_M3 + " integer);";

   private SQLiteHelper sqLiteHelper;
   private SQLiteDatabase sqLiteDatabase;

   private Context context;

   public dbStuff(Context c){
      context = c;
   }

   public dbStuff openToRead() throws android.database.SQLException {
      sqLiteHelper = new SQLiteHelper(context, MYDATABASE_NAME, null, MYDATABASE_VERSION);
      sqLiteDatabase = sqLiteHelper.getReadableDatabase();
      return this;
   }

   public dbStuff openToWrite() throws android.database.SQLException {
   sqLiteHelper = new SQLiteHelper(context, MYDATABASE_NAME, null, MYDATABASE_VERSION);
   sqLiteDatabase = sqLiteHelper.getWritableDatabase();
   return this;
   }

   public void close(){
      sqLiteHelper.close();
   }

   public int deleteAll(){
      return sqLiteDatabase.delete(MYDATABASE_TABLE, null, null);
   }


   public class SQLiteHelper extends SQLiteOpenHelper {

      public SQLiteHelper(Context context, String name,
         CursorFactory factory, int version) {
         super(context, name, factory, version);
      }

      @Override
      public void onCreate(SQLiteDatabase db) {
      
        db.execSQL(SCRIPT_CREATE_DATABASE);
        db.execSQL("insert into guide (id,subject,bunked,cps,m1,m2,m3) values(1,'sub1',0,1,0,0,0);");
        db.execSQL("insert into guide (id,subject,bunked,cps,m1,m2,m3) values(2,'sub2',0,1,0,0,0);");
        db.execSQL("insert into guide (id,subject,bunked,cps,m1,m2,m3) values(3,'sub3',0,1,0,0,0);");
        db.execSQL("insert into guide (id,subject,bunked,cps,m1,m2,m3) values(4,'sub4',0,1,0,0,0);");
        db.execSQL("insert into guide (id,subject,bunked,cps,m1,m2,m3) values(5,'sub5',0,1,0,0,0);");
        db.execSQL("insert into guide (id,subject,bunked,cps,m1,m2,m3) values(6,'sub6',0,1,0,0,0);");
      }

      @Override
      public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
       

      }

   }
   public String getSubj(int i) {
      String[] columns = new String[]{KEY_ID, KEY_SUBJECT, KEY_BUNKED, KEY_CPS, KEY_M1, KEY_M2, KEY_M3};
      Cursor c = sqLiteDatabase.query(MYDATABASE_TABLE, columns, KEY_ID + "=" + i, null, null, null, null);
      if(c != null) {
          c.moveToFirst();
          String subj = c.getString(1);
          return subj;
      }
      return null;
   }
   public String getBunk(int i) {
      String[] columns = new String[]{KEY_ID, KEY_SUBJECT, KEY_BUNKED, KEY_CPS, KEY_M1, KEY_M2, KEY_M3};
      Cursor ct = sqLiteDatabase.query(MYDATABASE_TABLE, columns, KEY_ID + "=" + i, null, null, null, null);
      if(ct != null) {
          ct.moveToFirst();
          String bunk = ct.getString(2);
          return bunk;
      }
      return null;
   }
   public String getCps(int i) {
      String[] columns = new String[]{KEY_ID, KEY_SUBJECT, KEY_BUNKED, KEY_CPS, KEY_M1, KEY_M2, KEY_M3};
      Cursor ct = sqLiteDatabase.query(MYDATABASE_TABLE, columns, KEY_ID + "=" + i, null, null, null, null);
      if(ct != null) {
          ct.moveToFirst();
          String cps = ct.getString(3);
          return cps;
      }
      return null;
   }
   public String getM1(int i) {
      String[] columns = new String[]{KEY_ID, KEY_SUBJECT, KEY_BUNKED, KEY_CPS, KEY_M1, KEY_M2, KEY_M3};
      Cursor ct = sqLiteDatabase.query(MYDATABASE_TABLE, columns, KEY_ID + "=" + i, null, null, null, null);
      if(ct != null) {
          ct.moveToFirst();
          String m1 = ct.getString(4);
          return m1;
      }
      return null;
   }
   public String getM2(int i) {
      String[] columns = new String[]{KEY_ID, KEY_SUBJECT, KEY_BUNKED, KEY_CPS, KEY_M1, KEY_M2, KEY_M3};
      Cursor ct = sqLiteDatabase.query(MYDATABASE_TABLE, columns, KEY_ID + "=" + i, null, null, null, null);
      if(ct != null) {
          ct.moveToFirst();
          String m2 = ct.getString(5);
          return m2;
      }
      return null;
   }
   public String getM3(int i) {
      String[] columns = new String[]{KEY_ID, KEY_SUBJECT, KEY_BUNKED, KEY_CPS, KEY_M1, KEY_M2, KEY_M3};
      Cursor ct = sqLiteDatabase.query(MYDATABASE_TABLE, columns, KEY_ID + "=" + i, null, null, null, null);
      if(ct != null) {
          ct.moveToFirst();
          String m3 = ct.getString(6);
          return m3;
      }
      return null;
   }

   public void modify(int row,String s,int x1,int x2,int x3,int x4,int x5) throws android.database.SQLException {
       ContentValues cvModify = new ContentValues();
       cvModify.put(KEY_SUBJECT,s);
       cvModify.put(KEY_BUNKED,x1);
       cvModify.put(KEY_CPS,x2);
       cvModify.put(KEY_M1,x3);
       cvModify.put(KEY_M2,x4);
       cvModify.put(KEY_M3,x5);
       sqLiteDatabase.update(MYDATABASE_TABLE,cvModify,KEY_ID + "=" + row, null);
   }
}

