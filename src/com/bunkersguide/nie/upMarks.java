package com.bunkersguide.nie;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.content.Intent;
import android.app.Dialog;
import android.widget.TextView;
import android.graphics.Typeface;

public class upMarks extends Activity implements OnClickListener
{
    TextView tvsub1,tvsub2,tvsub3,tvsub4,tvsub5,tvsub6;
    Button usave1,usave2,usave3,usave4,usave5,usave6;
    EditText s1m1,s2m1,s3m1,s4m1,s5m1,s6m1;
    EditText s1m2,s2m2,s3m2,s4m2,s5m2,s6m2;
    EditText s1m3,s2m3,s3m3,s4m3,s5m3,s6m3;
    String sub1s,sub2s,sub3s,sub4s,sub5s,sub6s;
    
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.upmarks);

        String med = "fonts/medium.ttf";
        Typeface medtf = Typeface.createFromAsset(getAssets(), med);
        
        usave1 = (Button) findViewById(R.id.xusave1);
        usave2 = (Button) findViewById(R.id.xusave2);
        usave3 = (Button) findViewById(R.id.xusave3);
        usave4 = (Button) findViewById(R.id.xusave4);
        usave5 = (Button) findViewById(R.id.xusave5);
        usave6 = (Button) findViewById(R.id.xusave6);
        usave1.setOnClickListener(this);
        usave2.setOnClickListener(this);
        usave3.setOnClickListener(this);
        usave4.setOnClickListener(this);
        usave5.setOnClickListener(this);
        usave6.setOnClickListener(this);
        usave1.setTypeface(medtf);
        usave2.setTypeface(medtf);
        usave3.setTypeface(medtf);
        usave4.setTypeface(medtf);
        usave5.setTypeface(medtf);
        usave6.setTypeface(medtf);

        tvsub1 = (TextView) findViewById(R.id.usub1);
        tvsub2 = (TextView) findViewById(R.id.usub2);
        tvsub3 = (TextView) findViewById(R.id.usub3);
        tvsub4 = (TextView) findViewById(R.id.usub4);
        tvsub5 = (TextView) findViewById(R.id.usub5);
        tvsub6 = (TextView) findViewById(R.id.usub6);

        s1m1 = (EditText) findViewById(R.id.us1m1);  
        s2m1 = (EditText) findViewById(R.id.us2m1);  
        s3m1 = (EditText) findViewById(R.id.us3m1);  
        s4m1 = (EditText) findViewById(R.id.us4m1);  
        s5m1 = (EditText) findViewById(R.id.us5m1);  
        s6m1 = (EditText) findViewById(R.id.us6m1);

        s1m2 = (EditText) findViewById(R.id.us1m2);  
        s2m2 = (EditText) findViewById(R.id.us2m2);  
        s3m2 = (EditText) findViewById(R.id.us3m2);  
        s4m2 = (EditText) findViewById(R.id.us4m2);  
        s5m2 = (EditText) findViewById(R.id.us5m2);  
        s6m2 = (EditText) findViewById(R.id.us6m2);

        s1m3 = (EditText) findViewById(R.id.us1m3);  
        s2m3 = (EditText) findViewById(R.id.us2m3);  
        s3m3 = (EditText) findViewById(R.id.us3m3);  
        s4m3 = (EditText) findViewById(R.id.us4m3);  
        s5m3 = (EditText) findViewById(R.id.us5m3);  
        s6m3 = (EditText) findViewById(R.id.us6m3);

        dbStuff forMarks = new dbStuff(upMarks.this);
          forMarks.openToWrite();

          sub1s = forMarks.getSubj(1);
          sub2s = forMarks.getSubj(2);
          sub3s = forMarks.getSubj(3);
          sub4s = forMarks.getSubj(4);
          sub5s = forMarks.getSubj(5);
          sub6s = forMarks.getSubj(6);

          String x1m1s = forMarks.getM1(1);
          String x2m1s = forMarks.getM1(2);
          String x3m1s = forMarks.getM1(3);
          String x4m1s = forMarks.getM1(4);
          String x5m1s = forMarks.getM1(5);
          String x6m1s = forMarks.getM1(6);

          String x1m2s = forMarks.getM2(1);
          String x2m2s = forMarks.getM2(2);
          String x3m2s = forMarks.getM2(3);
          String x4m2s = forMarks.getM2(4);
          String x5m2s = forMarks.getM2(5);
          String x6m2s = forMarks.getM2(6);

          String x1m3s = forMarks.getM3(1);
          String x2m3s = forMarks.getM3(2);
          String x3m3s = forMarks.getM3(3);
          String x4m3s = forMarks.getM3(4); 
          String x5m3s = forMarks.getM3(5);
          String x6m3s = forMarks.getM3(6);

          forMarks.close();

          tvsub1.setText(sub1s);
          tvsub2.setText(sub2s);
          tvsub3.setText(sub3s);
          tvsub4.setText(sub4s);
          tvsub5.setText(sub5s);
          tvsub6.setText(sub6s);
          
          s1m1.setText(x1m1s);
          s2m1.setText(x2m1s);
          s3m1.setText(x3m1s);
          s4m1.setText(x4m1s);
          s5m1.setText(x5m1s);
          s6m1.setText(x6m1s);
          s1m2.setText(x1m2s);
          s2m2.setText(x2m2s);
          s3m2.setText(x3m2s);
          s4m2.setText(x4m2s);
          s5m2.setText(x5m2s);
          s6m2.setText(x6m2s);
          s1m3.setText(x1m3s);
          s2m3.setText(x2m3s);
          s3m3.setText(x3m3s);
          s4m3.setText(x4m3s);
          s5m3.setText(x5m3s);
          s6m3.setText(x6m3s);
    }
     public void onClick(View but) {
        dbStuff Save = new dbStuff(this);
        Save.openToWrite();
        switch(but.getId()) {
            case R.id.xusave1: boolean presave1 = true;
                              try {                              
                                 String ucps1s = Save.getCps(1);
                                 String ubnk1s = Save.getBunk(1);
                                 String us1m1s = s1m1.getText().toString();
                                 String us1m2s = s1m2.getText().toString();
                                 String us1m3s = s1m3.getText().toString();
                                 long ucps1l = Long.parseLong(ucps1s);
                                 long ubnk1l = Long.parseLong(ubnk1s);
                                 long us1m1l = Long.parseLong(us1m1s);
                                 long us1m2l = Long.parseLong(us1m2s);
                                 long us1m3l = Long.parseLong(us1m3s);
                                 int ucps1i = (int) ucps1l;
                                 int ubnk1i = (int) ubnk1l;
                                 int us1m1i = (int) us1m1l;
                                 int us1m2i = (int) us1m2l;
                                 int us1m3i = (int) us1m3l;

                                 if((us1m1i >= 26)||(us1m2i >= 26)||(us1m3i >= 26)) {
                                    presave1 = false;
                                    Toast.makeText(getApplicationContext(), "Marks cannot be more then 25!" , Toast.LENGTH_LONG).show();
                                 }
                                 
                                 if(presave1) {
                                    Save.modify(1,sub1s,ubnk1i,ucps1i,us1m1i,us1m2i,us1m3i);
                                 }
                              }catch(Exception e) { 
                                 presave1 = false;
                                    Toast.makeText(getApplicationContext(), "Save to Database failed" , Toast.LENGTH_LONG).show();
                              }finally {
                                 if(presave1) {     
                                    Toast.makeText(getApplicationContext(), "Saved to Database" , Toast.LENGTH_LONG).show();
                                 }
                              }
               break;
            case R.id.xusave2: boolean presave2 = true;
                              try {                              
                                 String ucps2s = Save.getCps(2);
                                 String ubnk2s = Save.getBunk(2);
                                 String us2m1s = s2m1.getText().toString();
                                 String us2m2s = s2m2.getText().toString();
                                 String us2m3s = s2m3.getText().toString();
                                 long ucps2l = Long.parseLong(ucps2s);
                                 long ubnk2l = Long.parseLong(ubnk2s);
                                 long us2m1l = Long.parseLong(us2m1s);
                                 long us2m2l = Long.parseLong(us2m2s);
                                 long us2m3l = Long.parseLong(us2m3s);
                                 int ucps2i = (int) ucps2l;
                                 int ubnk2i = (int) ubnk2l;
                                 int us2m1i = (int) us2m1l;
                                 int us2m2i = (int) us2m2l;
                                 int us2m3i = (int) us2m3l;

                                 if((us2m1i >= 26)||(us2m2i >= 26)||(us2m3i >= 26)) {
                                    presave2 = false;
                                    Toast.makeText(getApplicationContext(), "Marks cannot be more then 25!" , Toast.LENGTH_LONG).show();
                                 }
                                 
                                 if(presave2) {
                                    Save.modify(2,sub2s,ubnk2i,ucps2i,us2m1i,us2m2i,us2m3i);
                                 }
                              }catch(Exception e) { 
                                 presave2 = false;
                                    Toast.makeText(getApplicationContext(), "Save to Database failed" , Toast.LENGTH_LONG).show();
                              }finally {
                                 if(presave2) {     
                                    Toast.makeText(getApplicationContext(), "Saved to Database" , Toast.LENGTH_LONG).show();
                                 }
                              }
               break;
            case R.id.xusave3: boolean presave3 = true;
                              try {                              
                                 String ucps3s = Save.getCps(3);
                                 String ubnk3s = Save.getBunk(3);
                                 String us3m1s = s3m1.getText().toString();
                                 String us3m2s = s3m2.getText().toString();
                                 String us3m3s = s3m3.getText().toString();
                                 long ucps3l = Long.parseLong(ucps3s);
                                 long ubnk3l = Long.parseLong(ubnk3s);
                                 long us3m1l = Long.parseLong(us3m1s);
                                 long us3m2l = Long.parseLong(us3m2s);
                                 long us3m3l = Long.parseLong(us3m3s);
                                 int ucps3i = (int) ucps3l;
                                 int ubnk3i = (int) ubnk3l;
                                 int us3m1i = (int) us3m1l;
                                 int us3m2i = (int) us3m2l;
                                 int us3m3i = (int) us3m3l;

                                 if((us3m1i >= 26)||(us3m2i >= 26)||(us3m3i >= 26)) {
                                    presave3 = false;
                                    Toast.makeText(getApplicationContext(), "Marks cannot be more then 25!" , Toast.LENGTH_LONG).show();
                                 }
                                 
                                 if(presave3) {
                                    Save.modify(3,sub3s,ubnk3i,ucps3i,us3m1i,us3m2i,us3m3i);
                                 }
                              }catch(Exception e) { 
                                 presave3 = false;
                                    Toast.makeText(getApplicationContext(), "Save to Database failed" , Toast.LENGTH_LONG).show();
                              }finally {
                                 if(presave3) {     
                                    Toast.makeText(getApplicationContext(), "Saved to Database" , Toast.LENGTH_LONG).show();
                                 }
                              }
               break;
            case R.id.xusave4: boolean presave4 = true;
                              try {                              
                                 String ucps4s = Save.getCps(4);
                                 String ubnk4s = Save.getBunk(4);
                                 String us4m1s = s4m1.getText().toString();
                                 String us4m2s = s4m2.getText().toString();
                                 String us4m3s = s4m3.getText().toString();
                                 long ucps4l = Long.parseLong(ucps4s);
                                 long ubnk4l = Long.parseLong(ubnk4s);
                                 long us4m1l = Long.parseLong(us4m1s);
                                 long us4m2l = Long.parseLong(us4m2s);
                                 long us4m3l = Long.parseLong(us4m3s);
                                 int ucps4i = (int) ucps4l;
                                 int ubnk4i = (int) ubnk4l;
                                 int us4m1i = (int) us4m1l;
                                 int us4m2i = (int) us4m2l;
                                 int us4m3i = (int) us4m3l;

                                 if((us4m1i >= 26)||(us4m2i >= 26)||(us4m3i >= 26)) {
                                    presave4 = false;
                                    Toast.makeText(getApplicationContext(), "Marks cannot be more then 25!" , Toast.LENGTH_LONG).show();
                                 }
                                 
                                 if(presave4) {
                                    Save.modify(4,sub4s,ubnk4i,ucps4i,us4m1i,us4m2i,us4m3i);
                                 }
                              }catch(Exception e) { 
                                 presave4 = false;
                                    Toast.makeText(getApplicationContext(), "Save to Database failed" , Toast.LENGTH_LONG).show();
                              }finally {
                                 if(presave4) {     
                                    Toast.makeText(getApplicationContext(), "Saved to Database" , Toast.LENGTH_LONG).show();
                                 }
                              }
               break;
            case R.id.xusave5: boolean presave5 = true;
                              try {                              
                                 String ucps5s = Save.getCps(5);
                                 String ubnk5s = Save.getBunk(5);
                                 String us5m1s = s5m1.getText().toString();
                                 String us5m2s = s5m2.getText().toString();
                                 String us5m3s = s5m3.getText().toString();
                                 long ucps5l = Long.parseLong(ucps5s);
                                 long ubnk5l = Long.parseLong(ubnk5s);
                                 long us5m1l = Long.parseLong(us5m1s);
                                 long us5m2l = Long.parseLong(us5m2s);
                                 long us5m3l = Long.parseLong(us5m3s);
                                 int ucps5i = (int) ucps5l;
                                 int ubnk5i = (int) ubnk5l;
                                 int us5m1i = (int) us5m1l;
                                 int us5m2i = (int) us5m2l;
                                 int us5m3i = (int) us5m3l;

                                 if((us5m1i >= 26)||(us5m2i >= 26)||(us5m3i >= 26)) {
                                    presave5 = false;
                                    Toast.makeText(getApplicationContext(), "Marks cannot be more then 25!" , Toast.LENGTH_LONG).show();
                                 }
                                 
                                 if(presave5) {
                                    Save.modify(5,sub5s,ubnk5i,ucps5i,us5m1i,us5m2i,us5m3i);
                                 }
                              }catch(Exception e) { 
                                 presave5 = false;
                                    Toast.makeText(getApplicationContext(), "Save to Database failed" , Toast.LENGTH_LONG).show();
                              }finally {
                                 if(presave5) {     
                                    Toast.makeText(getApplicationContext(), "Saved to Database" , Toast.LENGTH_LONG).show();
                                 }
                              }
               break;
            case R.id.xusave6: boolean presave6 = true;
                              try {                              
                                 String ucps6s = Save.getCps(6);
                                 String ubnk6s = Save.getBunk(6);
                                 String us6m1s = s6m1.getText().toString();
                                 String us6m2s = s6m2.getText().toString();
                                 String us6m3s = s6m3.getText().toString();
                                 long ucps6l = Long.parseLong(ucps6s);
                                 long ubnk6l = Long.parseLong(ubnk6s);
                                 long us6m1l = Long.parseLong(us6m1s);
                                 long us6m2l = Long.parseLong(us6m2s);
                                 long us6m3l = Long.parseLong(us6m3s);
                                 int ucps6i = (int) ucps6l;
                                 int ubnk6i = (int) ubnk6l;
                                 int us6m1i = (int) us6m1l;
                                 int us6m2i = (int) us6m2l;
                                 int us6m3i = (int) us6m3l;

                                 if((us6m1i >= 26)||(us6m2i >= 26)||(us6m3i >= 26)) {
                                    presave6 = false;
                                    Toast.makeText(getApplicationContext(), "Marks cannot be more then 25!" , Toast.LENGTH_LONG).show();
                                 }
                                 
                                 if(presave6) {
                                    Save.modify(6,sub6s,ubnk6i,ucps6i,us6m1i,us6m2i,us6m3i);
                                 }
                              }catch(Exception e) { 
                                 presave6 = false;
                                    Toast.makeText(getApplicationContext(), "Save to Database failed" , Toast.LENGTH_LONG).show();
                              }finally {
                                 if(presave6) {     
                                    Toast.makeText(getApplicationContext(), "Saved to Database" , Toast.LENGTH_LONG).show();
                                 }
                              }
               break;
        }
     }
}
