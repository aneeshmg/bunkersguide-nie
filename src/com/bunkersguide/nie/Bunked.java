package com.bunkersguide.nie;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.content.Intent;
import android.app.Dialog;
import android.widget.TextView;
import android.graphics.Typeface;

public class Bunked extends Activity implements OnClickListener
{
    Button b1,b2,b3,b4,b5,b6;
    String s1,s2,s3,s4,s5,s6;
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bunked);

        String med = "fonts/medium.ttf";
        Typeface medtf = Typeface.createFromAsset(getAssets(), med);

        TextView header = (TextView) findViewById(R.id.head);
        header.setTypeface(medtf);

        b1 = (Button) findViewById(R.id.sub1);
        b1.setOnClickListener(this);
        b1.setTypeface(medtf);
        b2 = (Button) findViewById(R.id.sub2);
        b2.setOnClickListener(this);
        b2.setTypeface(medtf);
        b3 = (Button) findViewById(R.id.sub3);
        b3.setOnClickListener(this);
        b3.setTypeface(medtf);
        b4 = (Button) findViewById(R.id.sub4);
        b4.setOnClickListener(this);
        b4.setTypeface(medtf);
        b5 = (Button) findViewById(R.id.sub5);
        b5.setOnClickListener(this);
        b5.setTypeface(medtf);
        b6 = (Button) findViewById(R.id.sub6);
        b6.setOnClickListener(this);
        b6.setTypeface(medtf);


       dbStuff buttonText = new dbStuff(this);

       buttonText.openToWrite();
       s1 = "Bunk " + buttonText.getSubj(1);
       s2 = "Bunk " + buttonText.getSubj(2);
       s3 = "Bunk " + buttonText.getSubj(3);
       s4 = "Bunk " + buttonText.getSubj(4);
       s5 = "Bunk " + buttonText.getSubj(5);
       s6 = "Bunk " + buttonText.getSubj(6);
       
       b1.setText(s1);
       b2.setText(s2);
       b3.setText(s3);
       b4.setText(s4);
       b5.setText(s5);
       b6.setText(s6);

       buttonText.close();

  }
       public void onClick(View but) {
          dbStuff Bunk = new dbStuff(this);
          Bunk.openToWrite();
          switch(but.getId()) {
              case R.id.sub1: try{
                                 String a1 = Bunk.getSubj(1);
                                 String b1 = Bunk.getBunk(1);
                                 String cpss1 = Bunk.getCps(1);
                                 String m1s1 = Bunk.getM1(1);
                                 String m2s1 = Bunk.getM2(1);
                                 String m3s1 = Bunk.getM3(1);
                                 long l1 = Long.parseLong(b1);
                                 long cpsl1 = Long.parseLong(cpss1);
                                 long m1l1 = Long.parseLong(m1s1);
                                 long m2l1 = Long.parseLong(m2s1);
                                 long m3l1 = Long.parseLong(m3s1);
                                 int L1 = (int) l1;
                                 int cpsi1 = (int) cpsl1;
                                 int m1i1 = (int) m1l1;
                                 int m2i1 = (int) m2l1;
                                 int m3i1 = (int) m3l1;
                                     L1 = L1 + 1;
                                 Bunk.modify(1,a1,L1,cpsi1,m1i1,m2i1,m3i1);
                              }catch(Exception e) {
                                 Toast.makeText(getApplicationContext(), "Operation Failed" , Toast.LENGTH_LONG).show();
                              }finally{
                                 String A1 = Bunk.getSubj(1);
                                 String B1 = Bunk.getBunk(1);
                                 long Q1 = Long.parseLong(B1);
                                 int q1 = (int) Q1;
                                 String m1 = "You have bunked " + A1 + " " + q1 + " times";
                                 Toast.makeText(getApplicationContext(), m1 , Toast.LENGTH_LONG).show();
                              }
                   break;
              case R.id.sub2: try{
                                 String a2 = Bunk.getSubj(2);
                                 String b2 = Bunk.getBunk(2);
                                 String cpss2 = Bunk.getCps(2);
                                 String m1s2 = Bunk.getM1(2);
                                 String m2s2 = Bunk.getM2(2);
                                 String m3s2 = Bunk.getM3(2);
                                 long l2 = Long.parseLong(b2);
                                 long cpsl2 = Long.parseLong(cpss2);
                                 long m1l2 = Long.parseLong(m1s2);
                                 long m2l2 = Long.parseLong(m2s2);
                                 long m3l2 = Long.parseLong(m3s2);
                                 int L2 = (int) l2;
                                 int cpsi2 = (int) cpsl2;
                                 int m1i2 = (int) m1l2;
                                 int m2i2 = (int) m2l2;
                                 int m3i2 = (int) m3l2;
                                     L2 = L2 + 1;
                                 Bunk.modify(2,a2,L2,cpsi2,m1i2,m2i2,m3i2);
                              }catch(Exception e) {
                                 Toast.makeText(getApplicationContext(), "Operation Failed" , Toast.LENGTH_LONG).show();
                              }finally{
                                 String A2 = Bunk.getSubj(2);
                                 String B2 = Bunk.getBunk(2);
                                 long Q2 = Long.parseLong(B2);
                                 int q2 = (int) Q2;
                                 String m2 = "You have bunked " + A2 + " " + q2 + " times";
                                 Toast.makeText(getApplicationContext(), m2 , Toast.LENGTH_LONG).show();
                              }

                   break;
              case R.id.sub3: try{
                                 String a3 = Bunk.getSubj(3);
                                 String b3 = Bunk.getBunk(3);
                                 String cpss3 = Bunk.getCps(3);
                                 String m1s3 = Bunk.getM1(3);
                                 String m2s3 = Bunk.getM2(3);
                                 String m3s3 = Bunk.getM3(3);
                                 long l3 = Long.parseLong(b3);
                                 long cpsl3 = Long.parseLong(cpss3);
                                 long m1l3 = Long.parseLong(m1s3);
                                 long m2l3 = Long.parseLong(m2s3);
                                 long m3l3 = Long.parseLong(m3s3);
                                 int L3 = (int) l3;
                                 int cpsi3 = (int) cpsl3;
                                 int m1i3 = (int) m1l3;
                                 int m2i3 = (int) m2l3;
                                 int m3i3 = (int) m3l3;
                                     L3 = L3 + 1;
                                 Bunk.modify(3,a3,L3,cpsi3,m1i3,m2i3,m3i3);
                              }catch(Exception e) {
                                 Toast.makeText(getApplicationContext(), "Operation Failed" , Toast.LENGTH_LONG).show();
                              }finally{
                                 String A3 = Bunk.getSubj(3);
                                 String B3 = Bunk.getBunk(3);
                                 long Q3 = Long.parseLong(B3);
                                 int q3 = (int) Q3;
                                 String m3 = "You have bunked " + A3 + " " + q3 + " times";
                                 Toast.makeText(getApplicationContext(), m3 , Toast.LENGTH_LONG).show();
                              }

                   break;
              case R.id.sub4: try{
                                 String a4 = Bunk.getSubj(4);
                                 String b4 = Bunk.getBunk(4);
                                 String cpss4 = Bunk.getCps(4);
                                 String m1s4 = Bunk.getM1(4);
                                 String m2s4 = Bunk.getM2(4);
                                 String m3s4 = Bunk.getM3(4);
                                 long l4 = Long.parseLong(b4);
                                 long cpsl4 = Long.parseLong(cpss4);
                                 long m1l4 = Long.parseLong(m1s4);
                                 long m2l4 = Long.parseLong(m2s4);
                                 long m3l4 = Long.parseLong(m3s4);
                                 int L4 = (int) l4;
                                 int cpsi4 = (int) cpsl4;
                                 int m1i4 = (int) m1l4;
                                 int m2i4 = (int) m2l4;
                                 int m3i4 = (int) m3l4;
                                     L4 = L4 + 1;
                                 Bunk.modify(4,a4,L4,cpsi4,m1i4,m2i4,m3i4);
                              }catch(Exception e) {
                                 Toast.makeText(getApplicationContext(), "Operation Failed" , Toast.LENGTH_LONG).show();
                              }finally{
                                 String A4 = Bunk.getSubj(4);
                                 String B4 = Bunk.getBunk(4);
                                 long Q4 = Long.parseLong(B4);
                                 int q4 = (int) Q4;
                                 String m4 = "You have bunked " + A4 + " " + q4 + " times";
                                 Toast.makeText(getApplicationContext(), m4 , Toast.LENGTH_LONG).show();
                              }

                   break;
              case R.id.sub5: try{
                                 String a5 = Bunk.getSubj(5);
                                 String b5 = Bunk.getBunk(5);
                                 String cpss5 = Bunk.getCps(5);
                                 String m1s5 = Bunk.getM1(5);
                                 String m2s5 = Bunk.getM2(5);
                                 String m3s5 = Bunk.getM3(5);
                                 long l5 = Long.parseLong(b5);
                                 long cpsl5 = Long.parseLong(cpss5);
                                 long m1l5 = Long.parseLong(m1s5);
                                 long m2l5 = Long.parseLong(m2s5);
                                 long m3l5 = Long.parseLong(m3s5);
                                 int L5 = (int) l5;
                                 int cpsi5 = (int) cpsl5;
                                 int m1i5 = (int) m1l5;
                                 int m2i5 = (int) m2l5;
                                 int m3i5 = (int) m3l5;
                                     L5 = L5 + 1;
                                 Bunk.modify(5,a5,L5,cpsi5,m1i5,m2i5,m3i5);
                              }catch(Exception e) {
                                 Toast.makeText(getApplicationContext(), "Operation Failed" , Toast.LENGTH_LONG).show();
                              }finally{
                                 String A5 = Bunk.getSubj(5);
                                 String B5 = Bunk.getBunk(5);
                                 long Q5 = Long.parseLong(B5);
                                 int q5 = (int) Q5;
                                 String m5 = "You have bunked " + A5 + " " + q5 + " times";
                                 Toast.makeText(getApplicationContext(), m5 , Toast.LENGTH_LONG).show();
                              }
 
                   break;
              case R.id.sub6: try{
                                 String a6 = Bunk.getSubj(6);
                                 String b6 = Bunk.getBunk(6);
                                 String cpss6 = Bunk.getCps(6);
                                 String m1s6 = Bunk.getM1(6);
                                 String m2s6 = Bunk.getM2(6);
                                 String m3s6 = Bunk.getM3(6);
                                 long l6 = Long.parseLong(b6);
                                 long cpsl6 = Long.parseLong(cpss6);
                                 long m1l6 = Long.parseLong(m1s6);
                                 long m2l6 = Long.parseLong(m2s6);
                                 long m3l6 = Long.parseLong(m3s6);
                                 int L6 = (int) l6;
                                 int cpsi6 = (int) cpsl6;
                                 int m1i6 = (int) m1l6;
                                 int m2i6 = (int) m2l6;
                                 int m3i6 = (int) m3l6;
                                     L6 = L6 + 1;
                                 Bunk.modify(6,a6,L6,cpsi6,m1i6,m2i6,m3i6);
                              }catch(Exception e) {
                                 Toast.makeText(getApplicationContext(), "Operation Failed" , Toast.LENGTH_LONG).show();
                              }finally{
                                 String A6 = Bunk.getSubj(6);
                                 String B6 = Bunk.getBunk(6);
                                 long Q6 = Long.parseLong(B6);
                                 int q6 = (int) Q6;
                                 String m6 = "You have bunked " + A6 + " " + q6 + " times";
                                 Toast.makeText(getApplicationContext(), m6 , Toast.LENGTH_LONG).show();
                              }

                   break;
          }
       Bunk.close();
       }

}
