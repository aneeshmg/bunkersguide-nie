package com.bunkersguide.nie;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.content.Intent;
import android.app.Dialog;
import android.widget.TextView;
import android.graphics.Typeface;

public class chkStatus extends Activity implements OnClickListener
{
    Button marks,att;
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chkstatus);
        String bold = "fonts/bold.ttf";
        Typeface boldtf = Typeface.createFromAsset(getAssets(), bold);
        marks = (Button) findViewById(R.id.xmarks);
        att = (Button) findViewById(R.id.xatt);
        marks.setOnClickListener(this);
        att.setOnClickListener(this);
        marks.setTypeface(boldtf);
        att.setTypeface(boldtf);
    }

    public void onClick(View but) {
        switch(but.getId()) {
            case R.id.xatt: 
                           Intent iat = new Intent("com.bunkersguide.nie.CHKATT");
                           startActivity(iat);
                break;
            case R.id.xmarks: 
                             Intent ma = new Intent("com.bunkersguide.nie.CHKMRK");
                             startActivity(ma);
                break;
        }
    }
}
