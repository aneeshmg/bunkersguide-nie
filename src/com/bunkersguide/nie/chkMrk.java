package com.bunkersguide.nie;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.content.Intent;
import android.app.Dialog;
import android.widget.TextView;
import android.widget.LinearLayout.LayoutParams;
import android.graphics.Typeface;

public class chkMrk extends Activity implements OnClickListener
{
    Button cmsub1,cmsub2,cmsub3,cmsub4,cmsub5,cmsub6;
    String cmb1,cmb2,cmb3,cmb4,cmb5,cmb6;
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chkmrk);
        String med = "fonts/medium.ttf";
        Typeface medtf = Typeface.createFromAsset(getAssets(), med);

        cmsub1 = (Button) findViewById(R.id.xcmsub1);
        cmsub2 = (Button) findViewById(R.id.xcmsub2);
        cmsub3 = (Button) findViewById(R.id.xcmsub3);
        cmsub4 = (Button) findViewById(R.id.xcmsub4);
        cmsub5 = (Button) findViewById(R.id.xcmsub5);
        cmsub6 = (Button) findViewById(R.id.xcmsub6);
        cmsub1.setOnClickListener(this);
        cmsub2.setOnClickListener(this);
        cmsub3.setOnClickListener(this);
        cmsub4.setOnClickListener(this);
        cmsub5.setOnClickListener(this);
        cmsub6.setOnClickListener(this);
        cmsub1.setTypeface(medtf);
        cmsub2.setTypeface(medtf);
        cmsub3.setTypeface(medtf);
        cmsub4.setTypeface(medtf);
        cmsub5.setTypeface(medtf);
        cmsub6.setTypeface(medtf);

        dbStuff last = new dbStuff(this);
        last.openToWrite();

          cmb1 = "Check " + last.getSubj(1) + " status";
          cmb2 = "Check " + last.getSubj(2) + " status";
          cmb3 = "Check " + last.getSubj(3) + " status";
          cmb4 = "Check " + last.getSubj(4) + " status";
          cmb5 = "Check " + last.getSubj(5) + " status";
          cmb6 = "Check " + last.getSubj(6) + " status";

        last.close();
       
        cmsub1.setText(cmb1);
        cmsub2.setText(cmb2);
        cmsub3.setText(cmb3);
        cmsub4.setText(cmb4);
        cmsub5.setText(cmb5);
        cmsub6.setText(cmb6);
    }
    public void onClick(View but) {
        dbStuff toCheck = new dbStuff(this);
        toCheck.openToWrite();
        switch(but.getId()) {
            case R.id.xcmsub1: boolean didItWork1 = true;
                               boolean isEligible1 = false;
                               String toShow1 = "Not Eligible";
                               try {
                                  String cm1s = toCheck.getM1(1);
                                  String cm2s = toCheck.getM2(1);
                                  String cm3s = toCheck.getM3(1);
                                  long cm1l = Long.parseLong(cm1s);
                                  long cm2l = Long.parseLong(cm2s);
                                  long cm3l = Long.parseLong(cm3s);
                                  int cm1i = (int) cm1l;
                                  int cm2i = (int) cm2l;
                                  int cm3i = (int) cm3l;
                                  int lar1;int lar2;
                                  if(cm1l <= cm2l) {
                                    lar1 = cm2i;
                                  }
                                  else {
                                    lar1 = cm1i;
                                  }
                                  if(cm2l <= cm3l) {
                                    lar2 = cm3i;
                                  }
                                  else if(cm1i <= cm3i) {
                                    lar2 = cm3i;
                                  }
                                  else {
                                    lar2 = cm1i;
                                  }
                                  int cie = lar1 + lar2;
                                  if( cie >= 25) { 
                                     isEligible1 = true;
                                     int forS = (90 - cie) * 2;
                                     int forA = (75 - cie) * 2;
                                     int forB = (60 - cie) * 2;
                                     int forC = (50 - cie) * 2;
                                     int forD = (40 - cie) * 2;
                                     String forSs = "Score " + forS + " for an S grade";
                                     String forAs = "Score " + forA + " for an A grade";
                                     String forBs = "Score " + forB + " for a B grade";
                                     String forCs = "Score " + forC + " for a C grade";
                                     String forDs = "Score " + forD + " for a D grade";
                                     toShow1 = forSs + "\n" + forAs + "\n" + forBs + "\n" + forCs + "\n" + forDs;
                                  }
                               }catch(Exception e) {
                                  didItWork1 = false;
                                  Toast.makeText(getApplicationContext(), "System Error!" , Toast.LENGTH_LONG).show();
                               }finally {
                                  if(didItWork1) {
                                     if(isEligible1) {
                                        Dialog d = new Dialog(this);
                                        d.getWindow().setLayout(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
                                        d.setTitle(toCheck.getSubj(1));
                                        TextView tv = new TextView(this);
                                        tv.setText(toShow1);
                                        d.setContentView(tv);
                                        d.show();
                                     }
                                     else Toast.makeText(getApplicationContext(), "Not Eligible to write your SEE" , Toast.LENGTH_LONG).show();
                                  }
                               }
               break;
            case R.id.xcmsub2: boolean didItWork2 = true;
                               boolean isEligible2 = false;
                               String toShow2 = "Not Eligible";
                               try {
                                  String cm1s = toCheck.getM1(2);
                                  String cm2s = toCheck.getM2(2);
                                  String cm3s = toCheck.getM3(2);
                                  long cm1l = Long.parseLong(cm1s);
                                  long cm2l = Long.parseLong(cm2s);
                                  long cm3l = Long.parseLong(cm3s);
                                  int cm1i = (int) cm1l;
                                  int cm2i = (int) cm2l;
                                  int cm3i = (int) cm3l;
                                  int lar1;int lar2;
                                  if(cm1l <= cm2l) {
                                    lar1 = cm2i;
                                  }
                                  else {
                                    lar1 = cm1i;
                                  }
                                  if(cm2l <= cm3l) {
                                    lar2 = cm3i;
                                  }
                                  else if(cm1i <= cm3i) {
                                    lar2 = cm3i;
                                  }
                                  else {
                                    lar2 = cm1i;
                                  }
                                  int cie = lar1 + lar2;
                                  if( cie >= 25) { 
                                     isEligible2 = true;
                                     int forS = (90 - cie) * 2;
                                     int forA = (75 - cie) * 2;
                                     int forB = (60 - cie) * 2;
                                     int forC = (50 - cie) * 2;
                                     int forD = (40 - cie) * 2;
                                     String forSs = "Score " + forS + " for an S grade";
                                     String forAs = "Score " + forA + " for an A grade";
                                     String forBs = "Score " + forB + " for a B grade";
                                     String forCs = "Score " + forC + " for a C grade";
                                     String forDs = "Score " + forD + " for a D grade";
                                     toShow2 = forSs + "\n" + forAs + "\n" + forBs + "\n" + forCs + "\n" + forDs;
                                  }
                               }catch(Exception e) {
                                  didItWork2 = false;
                                  Toast.makeText(getApplicationContext(), "System Error!" , Toast.LENGTH_LONG).show();
                               }finally {
                                  if(didItWork2) {
                                     if(isEligible2) {
                                        Dialog d = new Dialog(this);
                                        d.getWindow().setLayout(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
                                        d.setTitle(toCheck.getSubj(2));
                                        TextView tv = new TextView(this);
                                        tv.setText(toShow2);
                                        d.setContentView(tv);
                                        d.show();
                                     }
                                     else Toast.makeText(getApplicationContext(), "Not Eligible to write your SEE" , Toast.LENGTH_LONG).show();
                                  }
                               }
               break;
            case R.id.xcmsub3: boolean didItWork3 = true;
                               boolean isEligible3 = false;
                               String toShow3 = "Not Eligible";
                               try {
                                  String cm1s = toCheck.getM1(3);
                                  String cm2s = toCheck.getM2(3);
                                  String cm3s = toCheck.getM3(3);
                                  long cm1l = Long.parseLong(cm1s);
                                  long cm2l = Long.parseLong(cm2s);
                                  long cm3l = Long.parseLong(cm3s);
                                  int cm1i = (int) cm1l;
                                  int cm2i = (int) cm2l;
                                  int cm3i = (int) cm3l;
                                  int lar1;int lar2;
                                  if(cm1l <= cm2l) {
                                    lar1 = cm2i;
                                  }
                                  else {
                                    lar1 = cm1i;
                                  }
                                  if(cm2l <= cm3l) {
                                    lar2 = cm3i;
                                  }
                                  else if(cm1i <= cm3i) {
                                    lar2 = cm3i;
                                  }
                                  else {
                                    lar2 = cm1i;
                                  }
                                  int cie = lar1 + lar2;
                                  if( cie >= 25) { 
                                     isEligible3 = true;
                                     int forS = (90 - cie) * 2;
                                     int forA = (75 - cie) * 2;
                                     int forB = (60 - cie) * 2;
                                     int forC = (50 - cie) * 2;
                                     int forD = (40 - cie) * 2;
                                     String forSs = "Score " + forS + " for an S grade";
                                     String forAs = "Score " + forA + " for an A grade";
                                     String forBs = "Score " + forB + " for a B grade";
                                     String forCs = "Score " + forC + " for a C grade";
                                     String forDs = "Score " + forD + " for a D grade";
                                     toShow3 = forSs + "\n" + forAs + "\n" + forBs + "\n" + forCs + "\n" + forDs;
                                  }
                               }catch(Exception e) {
                                  didItWork3 = false;
                                  Toast.makeText(getApplicationContext(), "System Error!" , Toast.LENGTH_LONG).show();
                               }finally {
                                  if(didItWork3) {
                                     if(isEligible3) {
                                        Dialog d = new Dialog(this);
                                        d.getWindow().setLayout(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
                                        d.setTitle(toCheck.getSubj(3));
                                        TextView tv = new TextView(this);
                                        tv.setText(toShow3);
                                        d.setContentView(tv);
                                        d.show();
                                     }
                                     else Toast.makeText(getApplicationContext(), "Not Eligible to write your SEE" , Toast.LENGTH_LONG).show();
                                  }
                               }
               break;
            case R.id.xcmsub4: boolean didItWork4 = true;
                               boolean isEligible4 = false;
                               String toShow4 = "Not Eligible";
                               try {
                                  String cm1s = toCheck.getM1(4);
                                  String cm2s = toCheck.getM2(4);
                                  String cm3s = toCheck.getM3(4);
                                  long cm1l = Long.parseLong(cm1s);
                                  long cm2l = Long.parseLong(cm2s);
                                  long cm3l = Long.parseLong(cm3s);
                                  int cm1i = (int) cm1l;
                                  int cm2i = (int) cm2l;
                                  int cm3i = (int) cm3l;
                                  int lar1;int lar2;
                                  if(cm1l <= cm2l) {
                                    lar1 = cm2i;
                                  }
                                  else {
                                    lar1 = cm1i;
                                  }
                                  if(cm2l <= cm3l) {
                                    lar2 = cm3i;
                                  }
                                  else if(cm1i <= cm3i) {
                                    lar2 = cm3i;
                                  }
                                  else {
                                    lar2 = cm1i;
                                  }
                                  int cie = lar1 + lar2;
                                  if( cie >= 25) { 
                                     isEligible4 = true;
                                     int forS = (90 - cie) * 2;
                                     int forA = (75 - cie) * 2;
                                     int forB = (60 - cie) * 2;
                                     int forC = (50 - cie) * 2;
                                     int forD = (40 - cie) * 2;
                                     String forSs = "Score " + forS + " for an S grade";
                                     String forAs = "Score " + forA + " for an A grade";
                                     String forBs = "Score " + forB + " for a B grade";
                                     String forCs = "Score " + forC + " for a C grade";
                                     String forDs = "Score " + forD + " for a D grade";
                                     toShow4 = forSs + "\n" + forAs + "\n" + forBs + "\n" + forCs + "\n" + forDs;
                                  }
                               }catch(Exception e) {
                                  didItWork4 = false;
                                  Toast.makeText(getApplicationContext(), "System Error!" , Toast.LENGTH_LONG).show();
                               }finally {
                                  if(didItWork4) {
                                     if(isEligible4) {
                                        Dialog d = new Dialog(this);
                                        d.getWindow().setLayout(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
                                        d.setTitle(toCheck.getSubj(4));
                                        TextView tv = new TextView(this);
                                        tv.setText(toShow4);
                                        d.setContentView(tv);
                                        d.show();
                                     }
                                     else Toast.makeText(getApplicationContext(), "Not Eligible to write your SEE" , Toast.LENGTH_LONG).show();
                                  }
                               }
               break;
            case R.id.xcmsub5: boolean didItWork5 = true;
                               boolean isEligible5 = false;
                               String toShow5 = "Not Eligible";
                               try {
                                  String cm1s = toCheck.getM1(5);
                                  String cm2s = toCheck.getM2(5);
                                  String cm3s = toCheck.getM3(5);
                                  long cm1l = Long.parseLong(cm1s);
                                  long cm2l = Long.parseLong(cm2s);
                                  long cm3l = Long.parseLong(cm3s);
                                  int cm1i = (int) cm1l;
                                  int cm2i = (int) cm2l;
                                  int cm3i = (int) cm3l;
                                  int lar1;int lar2;
                                  if(cm1l <= cm2l) {
                                    lar1 = cm2i;
                                  }
                                  else {
                                    lar1 = cm1i;
                                  }
                                  if(cm2l <= cm3l) {
                                    lar2 = cm3i;
                                  }
                                  else if(cm1i <= cm3i) {
                                    lar2 = cm3i;
                                  }
                                  else {
                                    lar2 = cm1i;
                                  }
                                  int cie = lar1 + lar2;
                                  if( cie >= 25) { 
                                     isEligible5 = true;
                                     int forS = (90 - cie) * 2;
                                     int forA = (75 - cie) * 2;
                                     int forB = (60 - cie) * 2;
                                     int forC = (50 - cie) * 2;
                                     int forD = (40 - cie) * 2;
                                     String forSs = "Score " + forS + " for an S grade";
                                     String forAs = "Score " + forA + " for an A grade";
                                     String forBs = "Score " + forB + " for a B grade";
                                     String forCs = "Score " + forC + " for a C grade";
                                     String forDs = "Score " + forD + " for a D grade";
                                     toShow5 = forSs + "\n" + forAs + "\n" + forBs + "\n" + forCs + "\n" + forDs;
                                  }
                               }catch(Exception e) {
                                  didItWork5 = false;
                                  Toast.makeText(getApplicationContext(), "System Error!" , Toast.LENGTH_LONG).show();
                               }finally {
                                  if(didItWork5) {
                                     if(isEligible5) {
                                        Dialog d = new Dialog(this);
                                        d.getWindow().setLayout(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
                                        d.setTitle(toCheck.getSubj(5));
                                        TextView tv = new TextView(this);
                                        tv.setText(toShow5);
                                        d.setContentView(tv);
                                        d.show();
                                     }
                                     else Toast.makeText(getApplicationContext(), "Not Eligible to write your SEE" , Toast.LENGTH_LONG).show();
                                  }
                               }
               break;
            case R.id.xcmsub6: boolean didItWork6 = true;
                               boolean isEligible6 = false;
                               String toShow6 = "Not Eligible";
                               try {
                                  String cm1s = toCheck.getM1(6);
                                  String cm2s = toCheck.getM2(6);
                                  String cm3s = toCheck.getM3(6);
                                  long cm1l = Long.parseLong(cm1s);
                                  long cm2l = Long.parseLong(cm2s);
                                  long cm3l = Long.parseLong(cm3s);
                                  int cm1i = (int) cm1l;
                                  int cm2i = (int) cm2l;
                                  int cm3i = (int) cm3l;
                                  int lar1;int lar2;
                                  if(cm1l <= cm2l) {
                                    lar1 = cm2i;
                                  }
                                  else {
                                    lar1 = cm1i;
                                  }
                                  if(cm2l <= cm3l) {
                                    lar2 = cm3i;
                                  }
                                  else if(cm1i <= cm3i) {
                                    lar2 = cm3i;
                                  }
                                  else {
                                    lar2 = cm1i;
                                  }
                                  int cie = lar1 + lar2;
                                  if( cie >= 25) { 
                                     isEligible6 = true;
                                     int forS = (90 - cie) * 2;
                                     int forA = (75 - cie) * 2;
                                     int forB = (60 - cie) * 2;
                                     int forC = (50 - cie) * 2;
                                     int forD = (40 - cie) * 2;
                                     String forSs = "Score " + forS + " for an S grade";
                                     String forAs = "Score " + forA + " for an A grade";
                                     String forBs = "Score " + forB + " for a B grade";
                                     String forCs = "Score " + forC + " for a C grade";
                                     String forDs = "Score " + forD + " for a D grade";
                                     toShow6 = forSs + "\n" + forAs + "\n" + forBs + "\n" + forCs + "\n" + forDs;
                                  }
                               }catch(Exception e) {
                                  didItWork6 = false;
                                  Toast.makeText(getApplicationContext(), "System Error!" , Toast.LENGTH_LONG).show();
                               }finally {
                                  if(didItWork6) {
                                     if(isEligible6) {
                                        Dialog d = new Dialog(this);
                                        d.getWindow().setLayout(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
                                        d.setTitle(toCheck.getSubj(6));
                                        TextView tv = new TextView(this);
                                        tv.setText(toShow6);
                                        d.setContentView(tv);
                                        d.show();
                                     }
                                     else Toast.makeText(getApplicationContext(), "Not Eligible to write your SEE" , Toast.LENGTH_LONG).show();
                                  }
                               }
               break;
        }
        toCheck.close();
    }
}
