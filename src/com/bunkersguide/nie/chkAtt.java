package com.bunkersguide.nie;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.content.Intent;
import android.app.Dialog;
import android.widget.TextView;

public class chkAtt extends Activity
{
    TextView tv1,tv2,tv3,tv4,tv5,tv6;
    String x = "   ";
    String y = " ------ ";
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chkatt);
        tv1 = (TextView) findViewById(R.id.r1);
        tv2 = (TextView) findViewById(R.id.r2);
        tv3 = (TextView) findViewById(R.id.r3);
        tv4 = (TextView) findViewById(R.id.r4);
        tv5 = (TextView) findViewById(R.id.r5);
        tv6 = (TextView) findViewById(R.id.r6);

        dbStuff status = new dbStuff(this);
           status.openToWrite();
              String su1 = status.getSubj(1);
              String su2 = status.getSubj(2);
              String su3 = status.getSubj(3);
              String su4 = status.getSubj(4);
              String su5 = status.getSubj(5);
              String su6 = status.getSubj(6);

              String cpss1 = status.getCps(1);
              String cpss2 = status.getCps(2);
              String cpss3 = status.getCps(3);
              String cpss4 = status.getCps(4);
              String cpss5 = status.getCps(5);
              String cpss6 = status.getCps(6);
            
              String bnks1 = status.getBunk(1);
              String bnks2 = status.getBunk(2);
              String bnks3 = status.getBunk(3);
              String bnks4 = status.getBunk(4);
              String bnks5 = status.getBunk(5);
              String bnks6 = status.getBunk(6);

              long cpsl1 = Long.parseLong(cpss1);
              long cpsl2 = Long.parseLong(cpss2);
              long cpsl3 = Long.parseLong(cpss3);
              long cpsl4 = Long.parseLong(cpss4);
              long cpsl5 = Long.parseLong(cpss5);
              long cpsl6 = Long.parseLong(cpss6);

              long bnkl1 = Long.parseLong(bnks1);
              long bnkl2 = Long.parseLong(bnks2);
              long bnkl3 = Long.parseLong(bnks3);
              long bnkl4 = Long.parseLong(bnks4);
              long bnkl5 = Long.parseLong(bnks5);
              long bnkl6 = Long.parseLong(bnks6);

              long rl1 = (cpsl1-bnkl1)*100/cpsl1;
              long rl2 = (cpsl2-bnkl2)*100/cpsl2;
              long rl3 = (cpsl3-bnkl3)*100/cpsl3;
              long rl4 = (cpsl4-bnkl4)*100/cpsl4;
              long rl5 = (cpsl5-bnkl5)*100/cpsl5;
              long rl6 = (cpsl6-bnkl6)*100/cpsl6;

              String re1 = x + su1 + y + rl1 + "%";
              String re2 = x + su2 + y + rl2 + "%";
              String re3 = x + su3 + y + rl3 + "%";
              String re4 = x + su4 + y + rl4 + "%";
              String re5 = x + su5 + y + rl5 + "%";
              String re6 = x + su6 + y + rl6 + "%";

              tv1.setText(re1);
              tv2.setText(re2);
              tv3.setText(re3);
              tv4.setText(re4);
              tv5.setText(re5);
              tv6.setText(re6);

           status.close();
    }
}
