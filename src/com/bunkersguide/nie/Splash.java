package com.bunkersguide.nie;

import android.app.Activity;
import android.os.Bundle;
import android.content.Intent;
import android.media.MediaPlayer;

public class Splash extends Activity
{
    /** Called when the activity is first created. */
    @Override
    protected void onCreate(Bundle bg)
    {
        super.onCreate(bg);
        setContentView(R.layout.splash);
        MediaPlayer Intro = MediaPlayer.create(Splash.this, R.raw.intro);
        Intro.start();
        Thread timer = new Thread() {
           public void run() {
              boolean sp = true;
              try {
                 dbStuff splash = new dbStuff(Splash.this);
                 splash.openToWrite();
                 splash.close();
                 sleep(2000);
              } catch(InterruptedException e) {
                 sp = false;
                 e.printStackTrace();
              } finally {
                 if(sp) {
                   Intent o = new Intent("com.bunkersguide.nie.MAINACTIVITY");
                   startActivity(o);
                 }
              }
           }
        };
        timer.start();
    }
    @Override
    protected void onPause() {
       super.onPause();
       finish();
    }
}
