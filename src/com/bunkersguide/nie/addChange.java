package com.bunkersguide.nie;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.content.Intent;
import android.app.Dialog;
import android.widget.TextView;
import android.graphics.Typeface;

public class addChange extends Activity implements OnClickListener
{
    EditText asub1,asub2,asub3,asub4,asub5,asub6;
    EditText acps1,acps2,acps3,acps4,acps5,acps6;
    Button asave,arefresh;
    String ats1,ats2,ats3,ats4,ats5,ats6;
    String atcps1,atcps2,atcps3,atcps4,atcps5,atcps6;
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_change);
        asub1 = (EditText) findViewById(R.id.axsub1);
        asub2 = (EditText) findViewById(R.id.axsub2);
        asub3 = (EditText) findViewById(R.id.axsub3);
        asub4 = (EditText) findViewById(R.id.axsub4);
        asub5 = (EditText) findViewById(R.id.axsub5);
        asub6 = (EditText) findViewById(R.id.axsub6);
        
        acps1 = (EditText) findViewById(R.id.axcps1);
        acps2 = (EditText) findViewById(R.id.axcps2);
        acps3 = (EditText) findViewById(R.id.axcps3);
        acps4 = (EditText) findViewById(R.id.axcps4);
        acps5 = (EditText) findViewById(R.id.axcps5);
        acps6 = (EditText) findViewById(R.id.axcps6);

        String med = "fonts/medium.ttf";
        Typeface medtf = Typeface.createFromAsset(getAssets(), med);

        asave = (Button) findViewById(R.id.axsave);
        asave.setOnClickListener(this);
        arefresh = (Button) findViewById(R.id.axrefresh);
        arefresh.setOnClickListener(this);
        asave.setTypeface(medtf);
        arefresh.setTypeface(medtf);
        
        dbStuff addOrChange = new dbStuff(this);
        addOrChange.openToWrite();

          ats1 = addOrChange.getSubj(1);
          ats2 = addOrChange.getSubj(2);
          ats3 = addOrChange.getSubj(3);
          ats4 = addOrChange.getSubj(4);
          ats5 = addOrChange.getSubj(5);
          ats6 = addOrChange.getSubj(6);
          atcps1 = addOrChange.getCps(1);
          atcps2 = addOrChange.getCps(2);
          atcps3 = addOrChange.getCps(3);
          atcps4 = addOrChange.getCps(4);
          atcps5 = addOrChange.getCps(5);
          atcps6 = addOrChange.getCps(6);

          asub1.setText(ats1);
          asub2.setText(ats2);
          asub3.setText(ats3);
          asub4.setText(ats4);
          asub5.setText(ats5);
          asub6.setText(ats6);

          acps1.setText(atcps1);
          acps2.setText(atcps2);
          acps3.setText(atcps3);
          acps4.setText(atcps4);
          acps5.setText(atcps5);
          acps6.setText(atcps6);


        addOrChange.close();

    }

    public void onClick(View but) {
        switch(but.getId()) {
            case R.id.axsave: 
                              boolean diditwork = true;
                              try{
                               
                                 String ssub1 = asub1.getText().toString();
                                 String ssub2 = asub2.getText().toString();
                                 String ssub3 = asub3.getText().toString();
                                 String ssub4 = asub4.getText().toString();
                                 String ssub5 = asub5.getText().toString();
                                 String ssub6 = asub6.getText().toString();
                              
                                 String ccps1 = acps1.getText().toString();   
                                 String ccps2 = acps2.getText().toString();   
                                 String ccps3 = acps3.getText().toString();   
                                 String ccps4 = acps4.getText().toString();   
                                 String ccps5 = acps5.getText().toString();   
                                 String ccps6 = acps6.getText().toString();   
                              
                                 long cpsl1 = Long.parseLong(ccps1);
                                 long cpsl2 = Long.parseLong(ccps2);
                                 long cpsl3 = Long.parseLong(ccps3);
                                 long cpsl4 = Long.parseLong(ccps4);
                                 long cpsl5 = Long.parseLong(ccps5);
                                 long cpsl6 = Long.parseLong(ccps6);

                                 int cpsi1 = (int) cpsl1;
                                 int cpsi2 = (int) cpsl2;
                                 int cpsi3 = (int) cpsl3;
                                 int cpsi4 = (int) cpsl4;
                                 int cpsi5 = (int) cpsl5;
                                 int cpsi6 = (int) cpsl6;

                                 dbStuff entry = new dbStuff(addChange.this);

                                 entry.openToWrite();

                                   String abnk1s = entry.getBunk(1);
                                   String abnk2s = entry.getBunk(2);
                                   String abnk3s = entry.getBunk(3);
                                   String abnk4s = entry.getBunk(4);
                                   String abnk5s = entry.getBunk(5);
                                   String abnk6s = entry.getBunk(6);
                                   
                                   long abnk1l = Long.parseLong(abnk1s);
                                   long abnk2l = Long.parseLong(abnk2s);
                                   long abnk3l = Long.parseLong(abnk3s);
                                   long abnk4l = Long.parseLong(abnk4s);
                                   long abnk5l = Long.parseLong(abnk5s);
                                   long abnk6l = Long.parseLong(abnk6s);

                                   int abnk1i = (int) abnk1l;
                                   int abnk2i = (int) abnk2l;
                                   int abnk3i = (int) abnk3l;
                                   int abnk4i = (int) abnk4l;
                                   int abnk5i = (int) abnk5l;
                                   int abnk6i = (int) abnk6l;

                                  entry.modify(1,ssub1,abnk1i,cpsi1,0,0,0);                  
                                  entry.modify(2,ssub2,abnk2i,cpsi2,0,0,0);
                                  entry.modify(3,ssub3,abnk3i,cpsi3,0,0,0);
                                  entry.modify(4,ssub4,abnk4i,cpsi4,0,0,0);
                                  entry.modify(5,ssub5,abnk5i,cpsi5,0,0,0);
                                  entry.modify(6,ssub6,abnk6i,cpsi6,0,0,0);

                               entry.close();
                            }catch(Exception e){
                               diditwork = false;
                               Toast.makeText(getApplicationContext(), "Save to Database failed" , Toast.LENGTH_LONG).show();
                            }finally{
                               if(diditwork){
                                   Dialog d = new Dialog(this);
                                   d.setTitle("Done");
                                   TextView tv = new TextView(this);
                                   tv.setText("Subjects Added!");
                                   d.setContentView(tv);
                                   d.show();
                                }
                            }
                            break;
           case R.id.axrefresh: boolean rfresh = true;
                             try{
                                dbStuff Refresh = new dbStuff(this);
                                Refresh.openToWrite();
                          
                                   Refresh.modify(1,"Subject1",0,1,0,0,0);
                                   Refresh.modify(2,"Subject2",0,1,0,0,0);
                                   Refresh.modify(3,"Subject3",0,1,0,0,0);
                                   Refresh.modify(4,"Subject4",0,1,0,0,0);
                                   Refresh.modify(5,"Subject5",0,1,0,0,0);
                                   Refresh.modify(6,"Subject6",0,1,0,0,0);
                              
                                Refresh.close();
                             }catch(Exception e){
                                rfresh = false;
                                Toast.makeText(getApplicationContext(), "Failed to reset" , Toast.LENGTH_LONG).show();  
                             }finally{
                                if(rfresh)
                                   Toast.makeText(getApplicationContext(), "Database Reset" , Toast.LENGTH_LONG).show();
                             }
                            break;
       }
   }
}
