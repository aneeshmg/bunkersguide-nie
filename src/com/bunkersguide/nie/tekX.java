package com.bunkersguide.nie;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.graphics.Typeface;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class tekX extends Activity
{	
	Button txsite,txfb;
	TextView tekxa;
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {	
    	super.onCreate(savedInstanceState);
        setContentView(R.layout.tekx);
        String fp = "fonts/Comfortaa-Regular.ttf";
        Typeface tf = Typeface.createFromAsset(getAssets(), fp);
        tekxa = (TextView) findViewById(R.id.tex);
        tekxa.setTypeface(tf);
        txsite = (Button) findViewById(R.id.tekxsite);
        txfb = (Button) findViewById(R.id.tekxfb);
        txsite.setTypeface(tf);
        txfb.setTypeface(tf);
        txsite.setOnClickListener(new View.OnClickListener(){
		    public void onClick(View v){
		        Intent intent = new Intent();
		        intent.setAction(Intent.ACTION_VIEW);
		        intent.addCategory(Intent.CATEGORY_BROWSABLE);
		        intent.setData(Uri.parse("http://www.tekexpressions.com"));
		        startActivity(intent);
		    }
		});
        txfb.setOnClickListener(new View.OnClickListener(){
		    public void onClick(View v){
		        Intent intent = new Intent();
		        intent.setAction(Intent.ACTION_VIEW);
		        intent.addCategory(Intent.CATEGORY_BROWSABLE);
		        intent.setData(Uri.parse("http://www.facebook.com/tekexpressions"));
		        startActivity(intent);
		    }
		});
        
    }
}
