package com.bunkersguide.nie;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.content.Intent;
import android.app.Dialog;
import android.widget.TextView;
import android.graphics.Typeface;


public class mainActivity extends Activity implements OnClickListener
{
    Button add_change,bunked,chkstatus,cie,tek;
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        TextView header = (TextView) findViewById(R.id.welcome);
        add_change = (Button) findViewById(R.id.add_change);
        bunked = (Button) findViewById(R.id.bunked);
        chkstatus = (Button) findViewById(R.id.chkstatus);
        cie = (Button) findViewById(R.id.cie);
        tek = (Button) findViewById(R.id.tekx);

        String med = "fonts/medium.ttf";
        String bold = "fonts/bold.ttf";
        String light = "fonts/light.ttf";
        Typeface medtf = Typeface.createFromAsset(getAssets(), med);
        Typeface boldtf = Typeface.createFromAsset(getAssets(), bold);
        Typeface lighttf = Typeface.createFromAsset(getAssets(), light);

        add_change.setTypeface(boldtf);
        bunked.setTypeface(boldtf);
        chkstatus.setTypeface(boldtf);
        cie.setTypeface(boldtf);
        header.setTypeface(boldtf);
        add_change.setOnClickListener(this);
        bunked.setOnClickListener(this);
        chkstatus.setOnClickListener(this);
        cie.setOnClickListener(this);
        tek.setOnClickListener(this);
    }
    public void onClick(View but) {
        switch(but.getId()) {

           case R.id.add_change:
                                 Intent ac = new Intent("com.bunkersguide.nie.ADDCHANGE");
                                 startActivity(ac);
                                 break;
           case R.id.bunked:
                                 Intent bu = new Intent("com.bunkersguide.nie.BUNKED");
                                 startActivity(bu);
                                 break;
           case R.id.chkstatus:
                                 Intent cs = new Intent("com.bunkersguide.nie.CHKSTATUS");
                                 startActivity(cs);
                                 break;
           case R.id.cie:
                                 Intent ce = new Intent("com.bunkersguide.nie.UPMRK");
                                 startActivity(ce);
                                 break;
           case R.id.tekx:
                                 Intent tx = new Intent("com.bunkersguide.nie.TEKX");
                                 startActivity(tx);
                                 break;
        }
    }

}
